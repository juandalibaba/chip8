package com.juanda.chip8;

import java.util.Arrays;

public class Memory {
	
	private byte memory[] = new byte[4096];
	
	Memory(){
		reset();
	}
	
	private void checkWriteMemoryPosition(int pos) throws Exception{
		if(pos >= 0xFFF) {
			throw new Exception("maximum allowed memory position (0xFFF) exceded");
		}
		if(pos < 0x200) {
			throw new Exception("memory position can't be lower than 0x200");
		}
	}
	
	private void checkReadMemoryPosition(int pos) throws Exception{
		if(pos >= 0xFFF) {
			throw new Exception("maximum allowed memory position (0xFFF) exceded");
		}
		if(pos < 0) {
			throw new Exception("memory position can't be lower than 0x00");
		}
	}
	
	byte readInMemoryPosition(int pos) throws Exception {
		checkReadMemoryPosition(pos);
		return memory[pos];
	}
	
	void writeInMemoryPosition(byte d, int pos) throws Exception {
		checkWriteMemoryPosition(pos);
		memory[pos] = d;
	}
	
	void reset() {
		Arrays.fill(memory, (byte) 0);
		// character "0"
		memory[0x00] = (byte) 0xf0;
		memory[0x01] = (byte) 0x90;
		memory[0x02] = (byte) 0x90;
		memory[0x03] = (byte) 0x90;
		memory[0x04] = (byte) 0xf0;
		
		// character "1"
		memory[0x05] = (byte) 0x20;
		memory[0x06] = (byte) 0x60;
		memory[0x07] = (byte) 0x20;
		memory[0x08] = (byte) 0x20;
		memory[0x09] = (byte) 0x70;
		
		// character "2"
		memory[0x0A] = (byte) 0xf0;
		memory[0x0B] = (byte) 0x10;
		memory[0x0C] = (byte) 0xf0;
		memory[0x0D] = (byte) 0x80;
		memory[0x0E] = (byte) 0xf0;
		
		// character "3"
		memory[0x0F] = (byte) 0xf0;
		memory[0x10] = (byte) 0x10;
		memory[0x11] = (byte) 0xf0;
		memory[0x12] = (byte) 0x10;
		memory[0x13] = (byte) 0xf0;
		
		// character "4"
		memory[0x14] = (byte) 0x90;
		memory[0x15] = (byte) 0x90;
		memory[0x16] = (byte) 0xf0;
		memory[0x17] = (byte) 0x10;
		memory[0x18] = (byte) 0x10;
		
		// character "5"
		memory[0x19] = (byte) 0xf0;
		memory[0x1A] = (byte) 0x80;
		memory[0x1B] = (byte) 0xf0;
		memory[0x1C] = (byte) 0x10;
		memory[0x1D] = (byte) 0xf0;
		
		// character "6"
		memory[0x1E] = (byte) 0xf0;
		memory[0x1F] = (byte) 0x80;
		memory[0x20] = (byte) 0xf0;
		memory[0x21] = (byte) 0x90;
		memory[0x22] = (byte) 0xf0;
		
		// character "7"
		memory[0x23] = (byte) 0xf0;
		memory[0x24] = (byte) 0x10;
		memory[0x25] = (byte) 0x20;
		memory[0x26] = (byte) 0x40;
		memory[0x27] = (byte) 0x40;
		
		// character "8"
		memory[0x28] = (byte) 0xf0;
		memory[0x29] = (byte) 0x90;
		memory[0x2A] = (byte) 0xf0;
		memory[0x2B] = (byte) 0x90;
		memory[0x2C] = (byte) 0xf0;
		
		// character "9"
		memory[0x2D] = (byte) 0xf0;
		memory[0x2E] = (byte) 0x90;
		memory[0x2F] = (byte) 0xf0;
		memory[0x30] = (byte) 0x10;
		memory[0x31] = (byte) 0xf0;
		
		// character "A"
		memory[0x32] = (byte) 0xf0;
		memory[0x33] = (byte) 0x90;
		memory[0x34] = (byte) 0xf0;
		memory[0x35] = (byte) 0x90;
		memory[0x36] = (byte) 0x90;
		
		// character "B"
		memory[0x37] = (byte) 0xe0;
		memory[0x38] = (byte) 0x90;
		memory[0x39] = (byte) 0xe0;
		memory[0x3A] = (byte) 0x90;
		memory[0x3B] = (byte) 0xe0;
		
		// character "C"
		memory[0x3C] = (byte) 0xf0;
		memory[0x3D] = (byte) 0x80;
		memory[0x3E] = (byte) 0x80;
		memory[0x3F] = (byte) 0x80;
		memory[0x40] = (byte) 0xf0;
		
		// character "D"
		memory[0x41] = (byte) 0xe0;
		memory[0x42] = (byte) 0x90;
		memory[0x43] = (byte) 0x90;
		memory[0x44] = (byte) 0x90;
		memory[0x45] = (byte) 0xe0;
		
		// character "E"
		memory[0x46] = (byte) 0xf0;
		memory[0x47] = (byte) 0x80;
		memory[0x48] = (byte) 0xf0;
		memory[0x49] = (byte) 0x80;
		memory[0x4A] = (byte) 0xf0;

		// character "F"
		memory[0x4B] = (byte) 0xf0;
		memory[0x4C] = (byte) 0x80;
		memory[0x4D] = (byte) 0xf0;
		memory[0x4E] = (byte) 0x80;
		memory[0x4F] = (byte) 0x80;
	}
	
}

