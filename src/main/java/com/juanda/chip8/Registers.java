package com.juanda.chip8;

import java.util.Arrays;

public class Registers {
	
	private byte registers[] = new byte[16];
	private short i;
	private short pc;
	
	Registers(){
		reset();
	}
	
	private void checkRegisterIndex(int pos) throws Exception{
		if(pos >= 16) {
			throw new Exception("maximum allowed register index (16) exceded");
		}
		if(pos < 0) {
			throw new Exception("register index can't be lower than 0");
		}
	}
	
	
	byte readV(int pos) throws Exception {
		checkRegisterIndex(pos);
		return registers[pos];
	}
	
	void writeV(byte d, int pos) throws Exception {
		checkRegisterIndex(pos);
		registers[pos] = d;
	}
	
	short readI() {
		return i;
	}
	
	void writeI(short d) {
		i = d;
	}
	
	short readPC() {
		return pc;
	}
	
	void writePC(short d) {
		pc = d;
	}
	
	void reset() {
		Arrays.fill(registers, (byte) 0);
		i = 0;
		pc = 0;
	}
}
