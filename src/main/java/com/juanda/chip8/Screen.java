package com.juanda.chip8;

import java.util.Arrays;

public class Screen {
	
	private byte gfx[] = new byte[64 * 32];
	
	Screen() {
		clear();
	}
	
	private void checkPixelPosition(int x, int y) throws Exception {
		if (x >= 64) throw new Exception("x value must be lower than 64");
		if (x < 0) throw new Exception("x value must be greater than 0");
		if (y >= 32) throw new Exception("y value must be lower than 32");
		if (y < 0) throw new Exception("y value must be greater than 0");
	}
	
	void  clear() {
		Arrays.fill( gfx, (byte) 0);
	}
	
	void setPixel(int x, int y) throws Exception {
		checkPixelPosition(x,y);
		gfx[y * 64 + x] = 1;
	}
	
	void unsetPixel(int x, int y) throws Exception {
		checkPixelPosition(x,y);
		gfx[y * 64 + x] = 0;
	}
	
	byte getPixelValue(int x, int y) throws Exception {
		checkPixelPosition(x, y);
		return gfx[y * 64 + x];
	}

}
