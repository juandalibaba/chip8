package com.juanda.chip8.timer;

import java.util.Timer;

import javax.sound.sampled.LineUnavailableException;

public class Chip8Timer{
	
	protected byte time = 0;
	protected Chip8TimerTask timerTask;
	protected Timer timer;
	protected String name;
	
	public void setTime(byte t) throws LineUnavailableException {
		if(time == (byte) 0) {
			timer = new Timer();
			timerTask = new Chip8TimerTask(this);
			this.timer.schedule(timerTask, 0, 17);
		}
		time = t;
	}
	
	public void reset() throws LineUnavailableException{
		setTime((byte)0);	
	}
	
	
	public byte getTime() {
		return time;
	}
	
	public String getStringTime() {
		int t = time & 0xFF;
		return Integer.toString(t);
	}
	
	public void decrement() {
		time --;
	}


	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}	
	
}
