package com.juanda.chip8.timer;

import java.util.TimerTask;

public class Chip8TimerTask extends TimerTask {
	
	private Chip8Timer timer;
	
	Chip8TimerTask(Chip8Timer timer){
		this.timer = timer;
	}

	
	@Override
	public void run() {
		timer.decrement();
		System.out.println(String.format("Timer %s, time: %s", timer.getName(), this.timer.getStringTime()));
		if (timer.getTime() == 0) {
			this.timer.timer.cancel();
			if(this.timer instanceof SoundTimer) {
				((SoundTimer) this.timer).stopSound();
			}
		}
		
	}

}
