package com.juanda.chip8.timer;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundTimer extends Chip8Timer{
	
	Clip audioClip;
	
	public SoundTimer() throws UnsupportedAudioFileException, IOException, LineUnavailableException{
		setName("ST");
		File audioFile = new File("/Users/juanda/Proyectos/SpringBoot/juanda-chip8/src/main/java/com/juanda/chip8/timer/beep-11.wav");
		AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
		AudioFormat format = audioStream.getFormat();
		DataLine.Info info = new DataLine.Info(Clip.class, format);
		audioClip = (Clip) AudioSystem.getLine(info);
		audioClip.open(audioStream);
	}

	public void setTime(byte t) throws LineUnavailableException {
		super.setTime(t);
		initSound();
	}
	
	public void initSound() throws LineUnavailableException {
		System.out.println("PIIIIIIII!");
		audioClip.start();

	}
	
	public void stopSound() {
		System.out.println("Sound stopped");
		audioClip.stop();
	}
	
}
