package com.juanda.chip8;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MemoryTest {
	private Memory memo;
	
	@BeforeEach
	void init() {
		memo = new Memory();
	}
	
	@Test
	void testWriteReadPosGreaterThan0x200() throws Exception {
		memo.writeInMemoryPosition((byte) 8, 0x200);
		byte r = memo.readInMemoryPosition(0x200);
		assertEquals((byte) 8, r);
		
		memo.writeInMemoryPosition((byte) 240, 0x250);
		byte r1 = memo.readInMemoryPosition(0x250);
		assertEquals((byte) 240, r1);
	}
	
	@Test
	void testPosException() throws Exception {
		Exception exception = assertThrows(Exception.class , () -> {
			memo.writeInMemoryPosition((byte) 8, 6000);
		});
		String expectedMessage = "maximum allowed memory position (0xFFF) exceded";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testPosException2() throws Exception {
		Exception exception = assertThrows(Exception.class , () -> {
			memo.writeInMemoryPosition((byte) 8, 0x120);
		});
		String expectedMessage = "memory position can't be lower than 0x200";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testAllElementsInPosGreaterThan0x200AreZeroAfterCreation() throws Exception {
		
		boolean test = true;
		
		for(int i = 0x200; i < 0xFFF; i++) {
			test = test && memo.readInMemoryPosition(i) == 0;
		}
		
		assertTrue(test);
	}

	@Test
	void testFontsRangeMemory() throws Exception {
		Random r = new Random();
		int low = 0x00;
		int high = 0x4F;
		int pos1 = r.nextInt(high-low) + low;
		int pos2 = r.nextInt(high-low) + low;
		byte c1 = memo.readInMemoryPosition(pos1);
		byte c2 = memo.readInMemoryPosition(pos2);
		
		assertTrue(c1 != 0x00);
		assertTrue(c2 != 0x00);
	}
	
	@Test
	void testResetMemory() throws Exception {
		memo.writeInMemoryPosition((byte) 10, 0x210);
		memo.reset();
		boolean test = true;
		
		for(int i = 0x200; i < 0xFFF; i++) {
			test = test && memo.readInMemoryPosition(i) == 0;
		}
		
		assertTrue(test);
	}

}


