package com.juanda.chip8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RegistersTest {
	private Registers reg;
	
	@BeforeEach
	void init() {
		reg = new Registers();
	}
	
	@Test
	void testWriteRead() throws Exception {
		reg.writeV((byte) 8, 5);
		byte r = reg.readV(5);
		assertEquals((byte) 8, r);
	}
	
	
	@Test
	void testPosException() throws Exception {
		Exception exception = assertThrows(Exception.class , () -> {
			reg.writeV((byte) 8, 6000);
		});
		String expectedMessage = "maximum allowed register index (16) exceded";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testPosException2() throws Exception {
		Exception exception = assertThrows(Exception.class , () -> {
			reg.writeV((byte) 8, -3);
		});
		String expectedMessage = "register index can't be lower than 0";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testAllElementsAreZeroAfterCreation() throws Exception {
		
		boolean test = true;
		
		for(int i = 0; i < 16; i++) {
			test = test && reg.readV(i) == 0;
		}
		
		assertTrue(test);
	}
	
	@Test
	void testResetMemory() throws Exception {
		reg.writeV((byte) 65, 1);
		reg.reset();
		boolean test = true;
		
		for(int i = 0; i < 16; i++) {
			test = test && reg.readV(i) == 0;
		}
		
		test = test && reg.readI() == 0;
		test = test && reg.readPC() == 0;
		
		assertTrue(test);
	}
	
	@Test
	void testWriteReadI() {
		reg.writeI((short) 0x0AB);
		
		assertEquals((short) 0x0AB, reg.readI());
	}
	
	@Test
	void testWriteReadPC() {
		reg.writePC((short) 0x0AF);
		
		assertEquals((short) 0x0AF, reg.readPC());
	}

}


