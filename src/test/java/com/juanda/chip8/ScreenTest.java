package com.juanda.chip8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ScreenTest {
	
	private Screen screen;
	
	@BeforeEach
	void setUp() throws Exception {
		screen = new Screen();
	}

	@Test
	void testSetPixel() throws Exception {
		screen.setPixel(3,7);
		
		assertEquals((byte) 1, screen.getPixelValue(3, 7));
	}
	
	@Test
	void testClear() throws Exception {
		screen.setPixel(3,7);
		screen.clear();
		boolean test = true;
		for(int i = 0; i < 64; i++) {
			for (int j = 0; i < 32 ; i++) {
				test = test && screen.getPixelValue(i, j) == 0;
			}
		}
		
		assertTrue(test);
	}
	
	@Test
	void testLimitPositionsX1() throws Exception {
		
		Exception exception = assertThrows(Exception.class , () -> {
			screen.setPixel(67, 0);
		});
		String expectedMessage = "x value must be lower than 64";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testLimitPositionsX2() throws Exception {
		
		Exception exception = assertThrows(Exception.class , () -> {
			screen.setPixel(-7, 0);
		});
		String expectedMessage = "x value must be greater than 0";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testLimitPositionsY1() throws Exception {
		
		Exception exception = assertThrows(Exception.class , () -> {
			screen.setPixel(0, 37);
		});
		String expectedMessage = "y value must be lower than 32";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void testLimitPositionsY2() throws Exception {
		
		Exception exception = assertThrows(Exception.class , () -> {
			screen.setPixel(0, -7);
		});
		String expectedMessage = "y value must be greater than 0";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}

}
