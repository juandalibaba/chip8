package com.juanda.chip8.timer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import javax.sound.sampled.LineUnavailableException;

import org.junit.jupiter.api.Test;

class Chip8TimerTest {
	
	@Test
	void test() throws LineUnavailableException, InterruptedException {
		DelayTimer dt = new DelayTimer();
		dt.setName("prueba delay");
		dt.setTime((byte) 60);
		
		assertNotEquals((byte)0, dt.getTime());
		
		TimeUnit.SECONDS.sleep(2);
		
		assertEquals((byte)0, dt.getTime());
	}

}
